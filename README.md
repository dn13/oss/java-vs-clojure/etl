# Java vs Clojure: ETL

This repository shows how an ETL ("extract/transform/load") process could
be written.  Of course, this is an extremely simple example, but I wanted
to show the advantages of mapping data from one form to another in Clojure,
one of it strong points.

Clojure works around only some very basic datastructures (lists and maps)
but it offers a great number of functions for it.  See the [cheat sheet](https://clojure.org/api/cheatsheet)
for a taste of them.

## Java

Java is type oriented object-oriented (OO) language and as such is pretty
rigid about it.  Even two classes that share exacly the same structure
cannot be easily mapped to eachother.  This example illustrates that.  To
solve this, you could have them implement the same interface, but this only
works if you're in control of both classes, which is not always the case
(e.g. if you import from a 3rd party library).  Alternatively, tools like
[MapStruct](https://mapstruct.org/) can help, as they generate mapping
code.

But this is mostly remedying the problem of type incompatibilities in strongly
typed languages.  This example stays as close to the "vanilla" language
as possible.

```java
public Output convert(Input in) {
    return new Output(Optional.of(in)
		  .map(Input::getName)
		  .map(String::toUpperCase)
		  .orElse("unnamed"));
}
```

Most of the code is about defining the types to map, but this is where
the actual work happens.  It also shows how you can use the newer lambda
styles in Java to your advantage, to reduce the code verbosity.

## Clojure

All this is not necessary in Clojure, since functions are first-class citizens
and the idiomatic way of handling data is to put it in lists, vectors and maps.

```clojure
(defn etl
  "Converts from `in` to out"
  [in]
  (update in :name (fnil (memfn toUpperCase) "unnamed")))
```

This is the full mapping code.  Note that there are no type declarations,
we don't need them.  We could expand it with some [spec](https://clojure.org/guides/spec)
declarations if we want, it would make it a little clearer what kind of
input is expected.

It also shows how you can refer to native Java member functions as first-class
citizens (using `memfn` in this case), and how you can compose functions into
new functions.  In addition you can see the full power of immutable data structures,
you don't need to worry about changing your input structure, since by definition
it's immutable.

## Conclusion

This very small example shows how Clojure is very well suited to transform
data from one structure into another and how this flexibility allows for very
expressive ETL applications, that still remain concise.
