package com.dn;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.*;

class EtlTest {

    @Test
    void convertsFromInputToOutput() {
	var etl = new Etl();
	var in = new Etl.Input("Test input");

	var out = etl.convert(in);

	assertNotNull(out);
	assertEquals("TEST INPUT", out.getName());
    }

    @Test
    void worksForNullNames() {
	var etl = new Etl();
	var in = new Etl.Input(null);

	var out = etl.convert(in);

	assertNotNull(out);
    }
}
