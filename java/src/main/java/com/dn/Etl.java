package com.dn;

import java.util.Optional;

public class Etl {

    public Output convert(Input in) {
	return new Output(Optional.of(in)
			  .map(Input::getName)
			  .map(String::toUpperCase)
			  .orElse("unnamed"));
    }

    public static class Input {
	private final String name;

	public Input(String name) {
	    this.name = name;
	}

	public String getName() {
	    return name;
	}
    }

    public static class Output {
	private final String name;

	public Output(String name) {
	    this.name = name;
	}

	public String getName() {
	    return name;
	}
    }
}
