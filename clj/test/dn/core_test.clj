(ns dn.core-test
  (:require [midje.sweet :refer :all]
            [dn.core :as sut]))

(facts "about ETL"
       (fact "converts from in to out"
             (sut/etl {:name "test input"}) => {:name "TEST INPUT"})

       (fact "works for `nil` names"
             (sut/etl {}) => truthy))
