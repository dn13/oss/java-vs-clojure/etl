(ns dn.core)

(defn etl
  "Converts from `in` to out"
  [in]
  (update in :name (fnil (memfn toUpperCase) "unnamed")))
